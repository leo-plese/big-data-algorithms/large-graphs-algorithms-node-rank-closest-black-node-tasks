from sys import stdin

def print_closest_black_node(n_nodes, black_set, orig_to_dests_dict):

    for i in range(n_nodes):
        if i in black_set:
            print(i, 0)
            continue

        visited = set()

        proc_q = [i]
        visited.add(i)

        found_black = False
        dist_to_black = 1

        rmn_children = len(orig_to_dests_dict[i])+1
        new_children = 0
        next_black_inds = set()

        first_pass = True

        while len(proc_q) > 0:
            if dist_to_black > 10:
                break
            proc_n = proc_q.pop(0)

            node_children = orig_to_dests_dict[proc_n]

            rmn_children -= 1

            candidates = node_children.difference(visited)
            if not first_pass:
                new_children += len(candidates)

            next_black_inds |= black_set.intersection(candidates)
            proc_q.extend(candidates)
            visited |= candidates


            if first_pass:
                if len(next_black_inds)>0:
                    min_child_ind = min(next_black_inds)

                    print("%d %d" % (min_child_ind, dist_to_black))

                    found_black = True

                    break

                first_pass = False

            if rmn_children == 0:
                dist_to_black += 1

                rmn_children = new_children
                new_children = 0

                if len(next_black_inds) > 0:
                    min_child_ind = min(next_black_inds)

                    print("%d %d" % (min_child_ind, dist_to_black))

                    found_black = True

                    break

                next_black_inds = set()

        if not found_black:
            print("-1 -1")



def get_orig_to_dests_dict(lines, from_ind, to_ind, n_nodes):
    orig_to_dests_dict = {}
    for i in range(n_nodes):
        orig_to_dests_dict[i] = set()

    for i in range(from_ind, to_ind):
        n1, n2 = tuple(map(int, lines[i].split()))

        n1_dests = orig_to_dests_dict[n1]
        n1_dests.add(n2)
        orig_to_dests_dict[n1] = n1_dests

        n2_dests = orig_to_dests_dict[n2]
        n2_dests.add(n1)
        orig_to_dests_dict[n2] = n2_dests

    return orig_to_dests_dict

if __name__=="__main__":
    lines = stdin.readlines()
    l0 = lines[0].split()
    n_nodes, n_edges = int(l0[0]), int(l0[1])
    to_ind = n_nodes+1

    black_set = set()
    for i in range(1, to_ind):
        if bool(int(lines[i].strip())):
            black_set.add(i-1)


    from_ind = to_ind
    to_ind = from_ind+n_edges

    orig_to_dests_dict = get_orig_to_dests_dict(lines, from_ind, to_ind, n_nodes)

    print_closest_black_node(n_nodes, black_set, orig_to_dests_dict)