from sys import stdin

def calc_rank(r, r_init, beta, dest_to_orig_dict, n_nodes, dest_nodes_len_list):

    for j in range(n_nodes):
        pars = dest_to_orig_dict.get(j, [])
        r[j] = beta * sum([r_init[p] / dest_nodes_len_list[p] for p in pars])

    add_amount = (1-beta) / n_nodes
    for j in range(n_nodes):
        r[j] += add_amount


def print_node_rank(max_iter_queried, n_nodes, beta, query_node_iter_list, dest_to_orig_dict, dest_nodes_len_list):
    r0 = [1.0 / n_nodes] * n_nodes

    all_r_list = []

    r = r0[:]

    for i in range(max_iter_queried):

        r_init = r[:]

        calc_rank(r, r_init, beta, dest_to_orig_dict, n_nodes, dest_nodes_len_list)

        all_r_list.append(r[:])

        # if sum([abs(r[j] - r_init[j]) for j in range(n_nodes)]) < 1e-10:
        #     break



    for query in query_node_iter_list:
        n_i, t_i = query

        print("%.10f" % (all_r_list[t_i-1][n_i]))


def get_matrix_m(lines, to_ind):

    dest_nodes_list = [list(map(int, lines[i].split())) for i in range(1, to_ind)]
    dest_nodes_len_list = [len(d) for d in dest_nodes_list]

    return dest_nodes_list, dest_nodes_len_list

def get_dest_to_orig_dict(dest_nodes_list,n_nodes):
    dest_to_orig_dict={}

    for i in range(n_nodes):
        dest_nodes = dest_nodes_list[i]
        for d in dest_nodes:
            l=dest_to_orig_dict.get(d,set())
            l.add(i)
            dest_to_orig_dict[d] = l

    return dest_to_orig_dict

if __name__=="__main__":
    lines = stdin.readlines()
    l0 = lines[0].split()
    n_nodes, beta = int(l0[0]), float(l0[1])

    to_ind = n_nodes+1

    dest_nodes_list, dest_nodes_len_list = get_matrix_m(lines,to_ind)

    dest_to_orig_dict = get_dest_to_orig_dict(dest_nodes_list,n_nodes)



    q = int(lines[to_ind])
    from_ind = to_ind + 1
    to_ind = from_ind + q

    query_node_iter_list = []
    iter_queried = []
    for i in range(from_ind, to_ind):
        ni_ti = tuple(map(int, lines[i].split()))
        query_node_iter_list.append(ni_ti)
        iter_queried.append(ni_ti[1])

    print_node_rank(max(iter_queried),n_nodes, beta, query_node_iter_list, dest_to_orig_dict, dest_nodes_len_list)





